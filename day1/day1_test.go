package main

import (
	"testing"
)

func test(t *testing.T, input int, expected int) {
	got := CountFuel(input)
	if got != expected {
		t.Errorf("Got %d not %d", got, expected)
	}
}

func TestFuel(t *testing.T) {
	test(t, 12,2)
	test(t, 14, 2)
	test(t, 1969, 654)
	test(t, 100756, 33583)
}
