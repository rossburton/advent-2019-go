package main

import (
	"bufio"
	"fmt"
	"math"
	"strconv"
	"os"
)

func CountFuel(mass int) int {
	var total int = 0
	var fuel int = 0
	for mass > 0 {
		fuel = int(math.Floor((float64(mass / 3))) - 2)
		if fuel < 0 {
			fuel = 0
		}
		mass = fuel
		total += fuel
	}
	return total
}

func main() {
	var sum int = 0
	var scanner = bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		var mass, err = strconv.Atoi(scanner.Text())
		if err != nil {
			fmt.Printf("couldn't convert number: %v\n", err)
			return
		}
		sum += CountFuel(mass)
	}
	fmt.Printf("Need %d fuel\n", sum)
}

