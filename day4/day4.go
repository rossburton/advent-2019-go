package main

import "fmt"

// CheckPassword ...
func CheckPassword(password int) bool {
	s := fmt.Sprint(password)
	if len(s) != 6 {
		return false
	}

	foundDouble := false
	// iterate over
	for i := 1; i < len(s); i++ {
		last := s[i-1]
		this := s[i]
		if last == this {
			foundDouble = true
		}
		if this < last {
			return false
		}
	}
	return foundDouble
}

func main() {
	count := 0
	for i := 387638; i <= 919123; i++ {
		if CheckPassword(i) {
			count++
		}
	}
	fmt.Println("Found", count)
}
