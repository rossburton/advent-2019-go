package main

import (
	"testing"
)

func test(t *testing.T, password int, reallyIs bool) {
	is := CheckPassword(password)
	if is != reallyIs {
		t.Errorf("Got %v not %v for %d", is, reallyIs, password)
	}

}
func TestPassword(t *testing.T) {
	test(t, 111111, true)
	test(t, 223450, false)
	test(t, 123789, false)
}
