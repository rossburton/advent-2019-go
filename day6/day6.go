package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

// Planet ...
type Planet struct {
	orbits *Planet
	name   string
}

/* map of all names to planets
 * planet knows who it orbits
 * to solve, iterate map keys and count upwards
 */
var names = map[string]*Planet{}

// Parse ...
func Parse(reader io.Reader) {
	var scanner = bufio.NewScanner(reader)
	for scanner.Scan() {
		split := strings.SplitN(scanner.Text(), ")", 2)
		sunName := split[0]
		moonName := split[1]

		sun, ok := names[sunName]
		if !ok {
			sun = &Planet{nil, sunName}
			names[sunName] = sun
		}
		moon, ok := names[moonName]
		if !ok {
			moon = &Planet{sun, moonName}
			names[moonName] = moon
		} else {
			moon.orbits = sun
		}
	}
}

func countOrbits(count *int, planet *Planet) {
	if planet.orbits != nil {
		(*count)++
		countOrbits(count, planet.orbits)
	}
}

// OrbitCountChecksum ...
func OrbitCountChecksum() int {
	total := 0
	for _, v := range names {
		countOrbits(&total, v)
	}
	return total
}

// GetParents ...
func GetParents(planet *Planet) []*Planet {
	parents := make([]*Planet, 0)
	for planet.orbits != nil {
		parents = append(parents, planet.orbits)
		planet = planet.orbits
	}
	return parents
}

// GetParentNames ...
func GetParentNames(planet *Planet) []string {
	parents := make([]string, 0)
	for planet.orbits != nil {
		parents = append(parents, planet.orbits.name)
		planet = planet.orbits
	}
	return parents
}

// TransferOrbit ...
func TransferOrbit(from, to *Planet) int {
	for i, v := range GetParentNames(from) {
		for ii, vv := range GetParentNames(to) {
			if v == vv {
				return i + ii
			}
		}
	}
	panic("Can't find orbit")
}

func main() {
	Parse(os.Stdin)
	checksum := OrbitCountChecksum()
	fmt.Println("Orbit Count Checksum is", checksum)
	fmt.Println("Transfer orbits", TransferOrbit(names["YOU"], names["SAN"]))
}
