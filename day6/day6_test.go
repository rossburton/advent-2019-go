package main

import (
	"os"
	"strings"
	"testing"

	"gotest.tools/assert"
	is "gotest.tools/assert/cmp"
)

func TestParse(t *testing.T) {
	input := "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L"
	Parse(strings.NewReader(input))
	checksum := OrbitCountChecksum()
	assert.Equal(t, checksum, 42)
}

func TestParents(t *testing.T) {
	Parse(strings.NewReader("A)B\nB)C\nC)D\nD)E"))
	parents := GetParents(names["E"])
	assert.Equal(t, len(parents), 4)
	assert.Equal(t, parents[0].name, "D")
	assert.Equal(t, parents[1].name, "C")
	assert.Equal(t, parents[2].name, "B")
	assert.Equal(t, parents[3].name, "A")
}

func TestParentNames(t *testing.T) {
	Parse(strings.NewReader("A)B\nB)C\nC)D\nD)E"))
	parents := GetParentNames(names["E"])
	assert.Assert(t, is.DeepEqual(parents, []string{"D", "C", "B", "A"}))
}

func TestTransfer(t *testing.T) {
	Parse(strings.NewReader("COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN"))
	assert.Equal(t, TransferOrbit(names["YOU"], names["SAN"]), 4)
}

func TestDay6Part1(t *testing.T) {
	reader, _ := os.Open("orbits")
	Parse(reader)
	checksum := OrbitCountChecksum()
	assert.Equal(t, checksum, 204521)
}
