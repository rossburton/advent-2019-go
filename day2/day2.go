package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func run(program []int) int {
	ip := 0
decode:
	for {
		switch program[ip] {
		case 1:
			ip++
			a := program[ip]
			ip++
			b := program[ip]
			ip++
			target := program[ip]
			ip++
			program[target] = program[a] + program[b]
		case 2:
			ip++
			a := program[ip]
			ip++
			b := program[ip]
			ip++
			target := program[ip]
			ip++
			program[target] = program[a] * program[b]
		case 99:
			break decode
		default:
			fmt.Println("Invalid opcode", program[ip])
			return 0
		}
	}
	return program[0]
}

func load() []int {
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')

	input := strings.Split(text, ",")
	program := make([]int, len(input))
	for i, v := range input {
		parsed, _ := strconv.Atoi(v)
		program[i] = parsed
	}
	return program
}

func main() {
	program := load()

	for noun := 0; noun < 100; noun++ {
		for verb := 0; verb < 100; verb++ {
			memory := make([]int, len(program))
			copy(memory, program)
			memory[1] = noun
			memory[2] = verb
			output := run(memory)
			fmt.Println("Final calculation", output)
			if output == 19690720 {
				fmt.Printf("Inputs were %d and %d, magic sum %d\n", noun, verb, 100*noun+verb)
				return
			}
		}
	}
}
