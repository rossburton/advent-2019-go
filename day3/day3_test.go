package main

import (
	"testing"
)

func TestShortest(t *testing.T) {
	first := Parse("R75,D30,R83,U83,L12,D49,R71,U7,L72")
	second := Parse("U62,R66,U55,R34,D71,R55,D58,R83")
	distance := Shortest(first, second)
	if distance != 159 {
		t.Errorf("Got %d not %d", distance, 159)
	}

	first = Parse("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51")
	second = Parse("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")
	distance = Shortest(first, second)
	if distance != 135 {
		t.Errorf("Got %d not %d", distance, 135)
	}
}
