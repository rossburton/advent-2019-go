package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"

	mapset "github.com/deckarep/golang-set"
)

type coord struct {
	x int
	y int
}

func execute(ops []string) map[coord]int {
	coords := map[coord]int{}
	//coords := mapset.NewSet()
	var pos coord
	distance := 0
	for _, v := range ops {
		var dx, dy int
		// assuming ascii
		op := v[0]
		length, _ := strconv.Atoi(v[1:])
		switch op {
		case 'R':
			dx = 1
			dy = 0
		case 'L':
			dx = -1
			dy = 0
		case 'U':
			dx = 0
			dy = 1
		case 'D':
			dx = 0
			dy = -1
		}
		for i := 0; i < length; i++ {
			pos.x += dx
			pos.y += dy
			distance++
			coords[pos] = distance
		}
	}
	return coords
}

func manhatten(c coord) int {
	return int(math.Abs(float64(c.x)) + math.Abs(float64(c.y)))
}

func keys(dict map[coord]int) mapset.Set {
	keys := mapset.NewSet()
	for k := range dict {
		keys.Add(k)
	}
	return keys
}

// Shortest calculate shortest
func Shortest(firstOps, secondOps []string) int {
	// Parse the operations to a diction of coordinates that were visited and
	// distance to get there
	first := execute(firstOps)
	second := execute(secondOps)

	// Get the intersection of the coordinates that were visited, to identify
	// the crossing points
	crossings := keys(first).Intersect(keys(second))

	// Iterate over the crossings, creating a list of distances for each
	// crossing (the distance travelled by each wire)
	distances := make([]int, 0)
	it := crossings.Iterator()
	for elem := range it.C {
		coord := elem.(coord)
		distances = append(distances, first[coord]+second[coord])
	}

	// Sort and return the shortest
	sort.Ints(distances)
	return distances[0]
}

// Parse the input string
func Parse(input string) []string {
	return strings.Split(input, ",")
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')
	first := Parse(text)
	text, _ = reader.ReadString('\n')
	second := Parse(text)

	distance := Shortest(first, second)
	fmt.Println("Shortest is", distance)
}
