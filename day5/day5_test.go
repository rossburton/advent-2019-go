package main

import (
	"os"
	"testing"

	"gotest.tools/assert"
	is "gotest.tools/assert/cmp"
)

func TestDecode(t *testing.T) {
	op, mode1, mode2, mode3 := DecodeOpcode(1002)
	assert.Equal(t, op, OpMultiply)
	assert.Equal(t, mode1, ModePosition)
	assert.Equal(t, mode2, ModeImmediate)
	assert.Equal(t, mode3, ModePosition)
	assert.Equal(t, op, OpMultiply)
}

func TestFetch(t *testing.T) {
	program := []int{1, 2, 3, 4, 5}
	assert.Equal(t, Fetch(program, ModeImmediate, 42), 42)
	assert.Equal(t, Fetch(program, ModePosition, 3), 4)
}

func runHelperWithIO(t *testing.T, program, expected, input, expectedOutput []int) {
	var intcode Intcode
	intcode.memory = program
	intcode.input = input
	intcode.Run()
	if expected != nil {
		assert.Assert(t, is.DeepEqual(intcode.memory, expected))
	}
	if expectedOutput != nil {
		assert.Assert(t, is.DeepEqual(intcode.output, expectedOutput))
	}
}

func runHelper(t *testing.T, input, expected []int) {
	/* just call withIO with nils */
	var intcode Intcode
	intcode.memory = input
	intcode.Run()
	assert.Assert(t, is.DeepEqual(intcode.memory, expected))
}

func TestAdd(t *testing.T) {
	runHelper(t,
		[]int{OpAdd, 1, 1, 5, OpHalt, 0},
		[]int{OpAdd, 1, 1, 5, OpHalt, 2})
}

func TestAddMul(t *testing.T) {
	runHelper(t,
		[]int{1, 1, 1, 4, 99, 5, 6, 0, 99},
		[]int{30, 1, 1, 4, 2, 5, 6, 0, 99})
}

func TestMode(t *testing.T) {
	runHelper(t,
		[]int{1002, 4, 3, 4, 33},
		[]int{1002, 4, 3, 4, 99})
}

func TestRun(t *testing.T) {
	// Taken from Day 2 examples
	runHelper(t,
		[]int{1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50},
		[]int{3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50})

	runHelper(t,
		[]int{1, 1, 1, 4, 99, 5, 6, 0, 99},
		[]int{30, 1, 1, 4, 2, 5, 6, 0, 99})
}

func TestJump(t *testing.T) {
	runHelper(t,
		[]int{OpJumpIfTrue, 3, 4, 99, 5, OpAdd, 1, 2, 0, 99},
		[]int{7, 3, 4, 99, 5, OpAdd, 1, 2, 0, 99})

	runHelper(t,
		[]int{OpJumpIfFalse, 4, 5, 99, 0, 6, OpAdd, 1, 2, 0, 99},
		[]int{9, 4, 5, 99, 0, 6, OpAdd, 1, 2, 0, 99})
}

func TestOutput(t *testing.T) {
	runHelperWithIO(t, []int{OpOutput, 3, 99, 42}, nil, nil, []int{42})
	runHelperWithIO(t, []int{104, 42, 99}, nil, nil, []int{42})
}

func TestInput(t *testing.T) {
	runHelperWithIO(t, []int{OpInput, 3, 99, 0}, []int{OpInput, 3, 99, 42}, []int{42}, nil)
}

func TestDay5Part1(t *testing.T) {
	reader, _ := os.Open("input-part1")
	intcode := LoadFromReader(reader)
	intcode.input = []int{1}
	intcode.Run()
	assert.Assert(t, is.DeepEqual(intcode.output, []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 13285749}))
}

func TestDay5Part2(t *testing.T) {
	reader, _ := os.Open("input-part2")
	intcode := LoadFromReader(reader)
	intcode.input = []int{5}
	intcode.Run()
	assert.Assert(t, is.DeepEqual(intcode.output, []int{5000972}))
}
