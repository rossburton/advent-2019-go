package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// Intcode Computer
type Intcode struct {
	memory        []int
	ip            int
	input, output []int
}

// Op codes
const (
	OpAdd         = 1
	OpMultiply    = 2
	OpInput       = 3
	OpOutput      = 4
	OpJumpIfTrue  = 5
	OpJumpIfFalse = 6
	OpLessThan    = 7
	OpEquals      = 8
	OpHalt        = 99
)

// Param modes
const (
	ModePosition  = 0
	ModeImmediate = 1
)

// DecodeOpcode ...
func DecodeOpcode(i int) (int, int, int, int) {
	s := fmt.Sprintf("%05d", i)
	mode3, _ := strconv.Atoi(s[0:1])
	mode2, _ := strconv.Atoi(s[1:2])
	mode1, _ := strconv.Atoi(s[2:3])
	op, _ := strconv.Atoi(s[3:])
	return op, mode1, mode2, mode3
}

// Fetch ...
func Fetch(program []int, mode int, location int) int {
	switch mode {
	case ModePosition:
		return program[location]
	case ModeImmediate:
		return location
	default:
		panic(fmt.Sprintf("Unknown mode %d", mode))
	}
}

// Fetch ...
func (intcode *Intcode) Fetch(mode int) int {
	value := intcode.memory[intcode.ip]
	intcode.ip++
	switch mode {
	case ModeImmediate:
		return value
	case ModePosition:
		return intcode.memory[value]
	default:
		panic(fmt.Sprintf("Unknown mode %d", mode))
	}
}

// Run ...
func (intcode *Intcode) Run() {
	for intcode.ip < len(intcode.memory) {
		op, modeA, modeB, _ := DecodeOpcode(intcode.Fetch(ModeImmediate))
		switch op {
		case OpAdd:
			a := intcode.Fetch(modeA)
			b := intcode.Fetch(modeB)
			target := intcode.Fetch(ModeImmediate)
			intcode.memory[target] = a + b
		case OpMultiply:
			a := intcode.Fetch(modeA)
			b := intcode.Fetch(modeB)
			target := intcode.Fetch(ModeImmediate)
			intcode.memory[target] = a * b
		case OpInput:
			target := intcode.Fetch(ModeImmediate)
			intcode.memory[target] = intcode.input[0]
			intcode.input = intcode.input[1:]
		case OpOutput:
			a := intcode.Fetch(modeA)
			intcode.output = append(intcode.output, a)
		case OpJumpIfTrue:
			test := intcode.Fetch(modeA)
			dest := intcode.Fetch(modeB)
			if test != 0 {
				intcode.ip = dest
			}
		case OpJumpIfFalse:
			test := intcode.Fetch(modeA)
			dest := intcode.Fetch(modeB)
			if test == 0 {
				intcode.ip = dest
			}
		case OpLessThan:
			a := intcode.Fetch(modeA)
			b := intcode.Fetch(modeB)
			target := intcode.Fetch(ModeImmediate)
			if a < b {
				intcode.memory[target] = 1
			} else {
				intcode.memory[target] = 0
			}
		case OpEquals:
			a := intcode.Fetch(modeA)
			b := intcode.Fetch(modeB)
			target := intcode.Fetch(ModeImmediate)
			if a == b {
				intcode.memory[target] = 1
			} else {
				intcode.memory[target] = 0
			}
		case OpHalt:
			return
		default:
			panic(fmt.Sprintf("Invalid opcode %d", op))
		}
	}
}

// LoadFromReader ...
func LoadFromReader(reader io.Reader) Intcode {
	var intcode Intcode

	text, _ := bufio.NewReader(reader).ReadString('\n')

	input := strings.Split(text, ",")
	program := make([]int, len(input))
	for i, v := range input {
		parsed, _ := strconv.Atoi(v)
		program[i] = parsed
	}
	intcode.memory = program
	return intcode
}

func main() {
	intcode := LoadFromReader(os.Stdin)
	for _, v := range os.Args[1:] {
		i, _ := strconv.Atoi(v)
		intcode.input = append(intcode.input, i)
	}
	intcode.Run()
	for _, v := range intcode.output {
		fmt.Println("Output: ", v)
	}
}
